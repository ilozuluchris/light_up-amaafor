function offNav(){
  $('.side-nav').css('margin-left','-245px');
  $('.main-content-area').css('margin-left','0px');
  $('.menu-toggler').click(function(){
    onNav();
    //$(this).find('span').toggleClass('active');
  });
}
function onNav(){
  $('.side-nav').css('margin-left','0');
  $('.main-content-area').css('margin-left','242px');
  $('.menu-toggler').click(function(){
    offNav();
    //$(this).find('span').toggleClass('active');
  });
}

function storageAvailable(type) {
    try {
        var storage = window[type],
            x = '__storage_test__';
        storage.setItem(x, x);
        storage.removeItem(x);
        return true;
    }
    catch(e) {
        return e instanceof DOMException && (
            // everything except Firefox
            e.code === 22 ||
            // Firefox
            e.code === 1014 ||
            // test name field too, because code might not be present
            // everything except Firefox
            e.name === 'QuotaExceededError' ||
            // Firefox
            e.name === 'NS_ERROR_DOM_QUOTA_REACHED') &&
            // acknowledge QuotaExceededError only if there's something already stored
            storage.length !== 0;
    }
}

/**
 * Display an alert if browser can not use localstorage.
 */
function canUse(){
if (storageAvailable('localStorage')) {
  // Yippee! We can use localStorage awesomeness
  console.log('can use');
}
else {
    console.log('Cannot use');
 alert('You can not use this. Ensure you are not in incognito mode');
}   
}

function getCurrentVote(position){
    var name_of_radio_element = position+'_voted'; 
    var form = document.getElementById('form');
    var current_vote = form.elements[name_of_radio_element].value;
    console.log(current_vote);
    return current_vote;
}

function saveVote(position){
    /**
        Check if the postion has an entry yet.
            If it doesnot create one. If it does push another value to the array
    */
    var position_votes = JSON.parse(localStorage.getItem(position));
    console.log(position_votes);
    if (position_votes == null){
        console.log('NO votes before now');
        var array = [getCurrentVote(position)];
        localStorage.setItem(position, JSON.stringify(array));
    }
    else{
        position_votes.push(getCurrentVote(position))
        localStorage.setItem(position, JSON.stringify(position_votes));
    }
    alert('Your vote has been casted. Thank you.')
    return;
}

function countVote(position){
    var position_votes = JSON.parse(localStorage.getItem(position));
        if (position_votes == null){
        return [null];
    }
    else{
       var  arrayToCount = position_votes;
        var results = {};
        results['A'] = arrayToCount.filter(i => i === 'A').length;
        results['B'] = arrayToCount.filter(i => i === 'B').length;
        results['C'] = arrayToCount.filter(i => i === 'C').length;
        console.log(results);
        return results;
    }
}

function displayResults(position){
    var string = ''
    var all_votes = countVote(position);
    var results_table = document.getElementById('resultsTable')
    for (var option in all_votes) {
        string += "<tr><td>"+ option + "</td><td>" + all_votes[option] + "</td></tr>"
    }
    results_table.innerHTML = string;
}
var position_map = {
	"president":"President",
	"vicepresident": "Vice President",
	"secgen": "Secretary General",
	"asg": "Assistant Secretary General",
	"finsec":"Financial Secretary",
	"treasurer":"Treasurer",
	"librarian":"Librarian",
	"pro": "Pro / Provost"
}

function deleteVotes(position){
	localStorage.removeItem(position);
	alert("All votes for "+ position_map[position]+ " have been deleted")
}