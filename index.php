<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Light Up Umuokanne</title>
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
    <script type="text/javascript" src="assets/js/jquery3.1.js"></script>
</head>
<body style="background: #1c1d26; color: rgba(255, 255, 255, 0.75);">
	<div class="container">
		<div style="padding-top: 1%">
            <?php require"database.php";
            $total_amount = getTotalAmountContributed();
            echo '<h1 class="" style="color: dodgerblue">Total amount raised : ' . $total_amount. ' in naira </h1>' ;
            ?>
            <h2 class="page-header">Help Umuokanne Solve Their Electricity Challenge</h2>
            <p>
                Umuokanne is a community located in Ohaji-Egbema local government area of Imo state.
                <br>For the past 30 years, this village has witnessed power outage and have not seen a glimmer of light.
                <br>This occurrence has hindered the economic growth and development of the community.
                <br>Our Enactus team has developed a system that is capable of generating self-sustaining electricity for this community.
                <br>Help us achieve this. Light up Umuokanne.
            </p>
<br>
            <form>
                <div class="form-group">
                    <label for="customer_name">Your name:</label>
                    <input type="text" class="form-control" id="customer_name" value="Anonymous">

                </div>
                <div class="form-group">
                    <label for="amount_to_pay">Amount:</label>
                    <input type="number" class="form-control" id="amount_to_pay" min="500" value="1000">

                </div>
                <div class="form-group">
                    <label for="currency_to_pay">Currency:</label>
                    <select class="form-control" id="currency_to_pay">
                        <option name="currency" value="NGN" selected="selected">NGN</option>
                        <option name="currency" value="USD">USD</option>
                    </select>

                </div>
<!--                <script type="text/javascript" src="https://ravesandboxapi.flutterwave.com/flwv3-pug/getpaidx/api/flwpbf-inline.js"></script>-->
                <script src="https://api.ravepay.co/flwv3-pug/getpaidx/api/flwpbf-inline.js"></script>
                <button type="button" class="btn btn-primary" onclick="payWithRave()">Make Your donation</button>
            </form>

            <script>
                const API_publicKey = "FLWPUBK-8e5f9b25c93d6a1228aed1fce293928f-X";
                    function payWithRave() {
                        var amount_filled = document.getElementById('amount_to_pay').value  ;
                        var name_filled = document.getElementById('customer_name').value;
                        const amount_to_pay = amount_filled ? amount_filled : '1000';
                        var customer_name =  name_filled ? name_filled : 'Anonymous'  ;
                        var currency_paid = $( "select option:selected" ).val();
                        var trxref = "sig-"+ Math.random();
                        var x = getpaidSetup({
                            PBFPubKey: API_publicKey,
                            customer_email: "ilozuluchidiuso@gmail.com",
                            amount: amount_to_pay,
                            currency: currency_paid,
                            payment_method: "both",
                            pay_button_text:'Help Umuokanne',
                            txref: trxref,
                            onclose: function () {
                            },
                            callback: function (response) {
                                var txref = response.tx.txRef; // collect flwRef returned and pass to a 					server page to complete status check.
                                console.log("This is the response returned after a charge", response);
                                if (
                                    response.tx.chargeResponseCode == "00" ||
                                    response.tx.chargeResponseCode == "0"
                                ) {
                                    var location =  "/paymentverification.php?txref="+txref+
                                        '&amount='+amount_to_pay+'&currency='+currency_paid+'&name='+customer_name;
                                    window.location = location; // redirect to a success page
                                } else {
                                    alert("Sorry, " + customer_name + "transaction failed");
                                }

                                x.close(); // use this to close the modal immediately after payment.
                            }
                        });
                    }
            </script>
        </div>
    </div>
	 <footer class="page-footer font-small blue pt-4">
         <div class="footer-copyright text-center py-3">
         © Copyright 2018, All Rights Reserved. Developed by <a href="mailto:ilozuluchidiuso@gmail.com">Ilozulu Chris </a>for Enactus Futo.
        </div>
     </footer>

<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
</body>
</html>